﻿using CNS.ZOOM360.Entities.StoreProcedures.Predictions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Shared.StoreProcedures.Predictions
{
  public  interface IPredictionReachServices
    {
        public List<PredictionReachModel> GetPredictionReachData(string UserID, string WorkspaceId, string ClientId);
        Task<string> SavePredictionSetup(SavePredictionModel input);
    }
}
