﻿using CNS.ZOOM360.Entities.StoreProcedures.Predictions;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Shared.StoreProcedures.Predictions;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Services.StoreProcedures.Predictions
{
    public class PredictionReachServices : IPredictionReachServices
    {
        private readonly IRepositoryBase<PredictionReachModel> _PredictionReachRepositry;
        private readonly IRepositoryBase<SavePredictionModel> _predictionReachRepositrySave;
        public PredictionReachServices(IRepositoryBase<PredictionReachModel> PredictionReachRepositry,
            IRepositoryBase<SavePredictionModel> predictionReachRepositrySave)
        {
            _PredictionReachRepositry = PredictionReachRepositry;
            _predictionReachRepositrySave = predictionReachRepositrySave;
        }

        public List<PredictionReachModel> GetPredictionReachData(string UserID, string WorkspaceId, string ClientId)
        {
            object[] parameters = {
                new SqlParameter("@USER_ID",UserID),
                new SqlParameter("@WORKSPACE_ID",WorkspaceId),
                new SqlParameter("@CLIENT_ID",ClientId),
                new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000) { Direction = ParameterDirection.Output }
            };
            string spQuery = StoreProcedureConstants.Sp_GETPREDICTIONREACH + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
                 " @V_MESSAGE OUTPUT";
            List<PredictionReachModel> AiInsightGraphData = _PredictionReachRepositry.ExecuteQuery(spQuery, parameters).ToList();

            return AiInsightGraphData;
        }
        public async Task<string> SavePredictionSetup(SavePredictionModel input)
        {

            object[] parameters = {
                new SqlParameter("@USER_ID", input.userID),
                new SqlParameter("@WORKSPACE_ID", input.workspaceID),
                new SqlParameter("@CLIENT_ID", input.clientId),
                new SqlParameter("@SCRIPT_NAME", string.IsNullOrEmpty(input.scriptName) ? (object)DBNull.Value:input.scriptName),
                new SqlParameter("@AMOUNT_SPENT_USD", string.IsNullOrEmpty(input.AmountSpentUSD) ? (object)DBNull.Value:input.AmountSpentUSD),
                new SqlParameter("@RESULTS", string.IsNullOrEmpty(input.results) ? (object)DBNull.Value:input.results),
                new SqlParameter("@IMPRESSIONS", string.IsNullOrEmpty(input.impressions) ? (object)DBNull.Value:input.impressions),
                new SqlParameter("@PURCHASES_CONVERSION_VALUE_USD", string.IsNullOrEmpty(input.purchaseConversionValueUSD) ? (object)DBNull.Value:input.purchaseConversionValueUSD),
                new SqlParameter("@REACH", string.IsNullOrEmpty(input.reach) ? (object)DBNull.Value:input.reach),
                new SqlParameter("@RESULT_TYPE_CODE",string.IsNullOrEmpty(input.resultTypeCode) ? (object)DBNull.Value: input.resultTypeCode),
                new SqlParameter("@PURCHASE_ROAS_RETURN_ON_AD_SPEND",string.IsNullOrEmpty(input.purchaseROASReturnonAdSpend) ? (object)DBNull.Value: input.purchaseROASReturnonAdSpend),
                new SqlParameter("@COST_PER_RESULT_USD",string.IsNullOrEmpty(input.costPerResultUSD) ? (object)DBNull.Value: input.costPerResultUSD),
                new SqlParameter("@STATUS",string.IsNullOrEmpty(input.Status) ? (object)DBNull.Value: input.Status),
                new SqlParameter("@PREDICTED_RESULTS",string.IsNullOrEmpty(input.PredictedResult) ? (object)DBNull.Value: input.PredictedResult),
                //new SqlParameter("@CLIENT_DATE",string.IsNullOrEmpty(input.ClientDate) ? (object)DBNull.Value: input.ClientDate),
            //new SqlParameter("@CLIENT_TIME   ",string.IsNullOrEmpty(input.ClientTime) ? (object)DBNull.Value: input.ClientTime),
            //new SqlParameter("@CLIENT_TIME_ZONE",string.IsNullOrEmpty(input.ClientTimeZone) ? (object)DBNull.Value: input.ClientTimeZone),
            //new SqlParameter("@REMARKS_1",string.IsNullOrEmpty(input.Remark1) ? (object)DBNull.Value: input.Remark1),
            //new SqlParameter("@REMARKS_2",string.IsNullOrEmpty(input.Remark2) ? (object)DBNull.Value: input.Remark2),
            //new SqlParameter("@REMARKS_3",string.IsNullOrEmpty(input.Remark3) ? (object)DBNull.Value: input.Remark3),
            //new SqlParameter("@REMARKS_4",string.IsNullOrEmpty(input.Remark4) ? (object)DBNull.Value: input.Remark4),
            //new SqlParameter("@FLEX_1",string.IsNullOrEmpty(input.Flex1) ? (object)DBNull.Value: input.Flex1),
            //new SqlParameter("@FLEX_2",string.IsNullOrEmpty(input.Flex2) ? (object)DBNull.Value: input.Flex2),
            //new SqlParameter("@FLEX_3",string.IsNullOrEmpty(input.Flex3) ? (object)DBNull.Value: input.Flex3),
            //new SqlParameter("@FLEX_4",string.IsNullOrEmpty(input.Flex4) ? (object)DBNull.Value: input.Flex4),
            //new SqlParameter("@FLEX_5",string.IsNullOrEmpty(input.Flex5) ? (object)DBNull.Value: input.Flex5),
            //new SqlParameter("@FLEX_6",string.IsNullOrEmpty(input.Flex6) ? (object)DBNull.Value: input.Flex6),
            //new SqlParameter("@FLEX_7",string.IsNullOrEmpty(input.Flex7) ? (object)DBNull.Value: input.Flex7),
            //new SqlParameter("@FLEX_8",string.IsNullOrEmpty(input.Flex8) ? (object)DBNull.Value: input.Flex8),
            //new SqlParameter("@FLEX_9",string.IsNullOrEmpty(input.Flex9) ? (object)DBNull.Value: input.Flex8),
            //new SqlParameter("@FLEX_10",string.IsNullOrEmpty(input.Flex10) ? (object)DBNull.Value: input.Flex10),
            //new SqlParameter("@FLEX_11",string.IsNullOrEmpty(input.Flex11) ? (object)DBNull.Value: input.Flex11),
            //new SqlParameter("@FLEX_12",string.IsNullOrEmpty(input.Flex12) ? (object)DBNull.Value: input.Flex12),
            //new SqlParameter("@FLEX_13",string.IsNullOrEmpty(input.Flex13) ? (object)DBNull.Value: input.Flex13),
            //new SqlParameter("@FLEX_14",string.IsNullOrEmpty(input.Flex14) ? (object)DBNull.Value: input.Flex14),
            //new SqlParameter("@FLEX_15",string.IsNullOrEmpty(input.Flex15) ? (object)DBNull.Value: input.Flex15),
            //new SqlParameter("@FLEX_16",string.IsNullOrEmpty(input.Flex16) ? (object)DBNull.Value: input.Flex16),

                new SqlParameter{ ParameterName = "@V_MESSAGE",
            Direction = ParameterDirection.Output,
            SqlDbType = SqlDbType.NVarChar,
            Size = 4000,
            Value = ""
                }

        };

            string spQuery = StoreProcedureConstants.Sp_SavePrediction + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID,  @SCRIPT_NAME,@AMOUNT_SPENT_USD, @RESULTS, @IMPRESSIONS, @PURCHASES_CONVERSION_VALUE_USD, @REACH, @RESULT_TYPE_CODE " +
                ", @PURCHASE_ROAS_RETURN_ON_AD_SPEND, @COST_PER_RESULT_USD, @STATUS, @PREDICTED_RESULTS," +
                //" @CLIENT_DATE,@CLIENT_TIME, @CLIENT_TIME_ZONE," +
                //" @REMARKS_1, @REMARKS_2, @REMARKS_3, @REMARKS_4, @FLEX_1, @FLEX_2, @FLEX_3,@FLEX_4,@FLEX_5, @FLEX_6," +
                //" @FLEX_7, @FLEX_8, @FLEX_9, @FLEX_10, @FLEX_11, @FLEX_12, @FLEX_13, @FLEX_14, @FLEX_15, @FLEX_16 ," +
                " @V_MESSAGE OUTPUT";
            return _predictionReachRepositrySave.ExecuteCommand(spQuery, parameters);

        }
    }
}
